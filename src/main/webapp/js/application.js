

//Crea una tabla dinámicamente a partir de un json
function createTableWithJSON(json,oldTableID) {

    var oldTable = document.getElementById(oldTableID),
        newTable = oldTable.cloneNode();

    //Array con cabeceras para la tabla
    var keys = Object.keys(json);

    //var markedTds = [ "one", "two", "three", "four", "five" ];

    //Cuerpo principal de la tabla
    var tbody = document.createElement('tbody');

    for(var i = 0; i < keys.length; i++){

        var tr = document.createElement('tr');

        if(i==0) tr.className = "success"; // Fila para días o meses

        for(var j = 0 ; j < json[keys[i]].length+1 ; j++){
            if (j==0){ //Filas para cabeceras
                var dataForTd = keys[i];
                //Espacios marcadas
                if (keys[i]=="Spaceone" || keys[i]=="Spacetwo"){
                    tr.className = "success";
                    dataForTd = "";
                }
                //Cabeceras marcadas
                if (keys[i]=="Desglose por Opciones de atención en lenguas cooficiales"
                    || keys[i]=="Llamadas finales recibidas por IVR"
                    || keys[i]=="Resultado gestión IVR"
                    || keys[i]=="Desglose TRANSFERENCIAS AGENTE (Dentro Horario)"
                    || keys[i]=="SMS"
                    || keys[i]=="Solicitud envío sms por confirmación de cita desde IVR"){

                    tr.className = "success";
                }

                // jQuery.each( markedTds, function( i, val ) {
                //     $( "#" + val ).text( "Mine is " + val + "." );
                // });

                var td = document.createElement('td');
                td.className = "tdLeftHead";
                td.style.width = '200px';
                td.appendChild(document.createTextNode(dataForTd));
                tr.appendChild(td);

                dataForTd = null;
            }
            else { //Datos
                var td = document.createElement('td');
                td.appendChild(document.createTextNode(json[keys[i]][j-1]));
                tr.appendChild(td);
            }
        }

        tbody.appendChild(tr);
    }

    //Cambio en el DOM de la tabla vacía por la nueva
    newTable.appendChild(tbody);
    oldTable.parentNode.replaceChild(newTable, oldTable);
}

//Devuelve los nombres de las variables dentro de un objeto
function getJSONKeys(obj) {
    var getKeys = function(obj){
        var keys = [];
        for(var key in obj){
            keys.push(key);
        }
        return keys;
    }
}

//Crea una tabla dinámicamente a partir de un json (Con las cabeceras arriba en vez de a la izquierda)
function createInverseTableWithJSON(json,oldTableID) {

    var oldTable = document.getElementById(oldTableID),
    newTable = oldTable.cloneNode(true);

    var keys = ["Ani","Nombre","Fecha de inserción"];
    var tbody = document.createElement('tbody');

    var firstLoop = false;
    for(var i = 0; i < json.length; i++){
        if(i==0 && firstLoop==false) {
        	var tr = document.createElement('tr');
            tr.className = "success";
            for(var k = 0; k < 3; k++){
                var td = document.createElement('td');
                td.appendChild(document.createTextNode(keys[k]));
                tr.appendChild(td);
            }
            newTable.appendChild(tr);
            tbody.appendChild(tr);
        }
        var tr = document.createElement('tr');
        for(var j = 0; j < 3; j++){
            var td = document.createElement('td');
            td.appendChild(document.createTextNode(json[i][keys[j]]));
            tr.appendChild(td);
        }

        newTable.appendChild(tr);
        tbody.appendChild(tr);
    }
    newTable.appendChild(tbody);
    oldTable.parentNode.replaceChild(newTable, oldTable);
    console.log(newTable);
}



//Redondea hasta decenas,centenas etc...
//alert(roundTo(1532, 100)); // 1500
//alert(roundTo(26, 10)); // 30
function roundTo(number, to) {
    return to * Math.floor(number / to);
}

//Comprueba si los input para las fechas no están vacíos
function checkEmptyDates() {
    if( !$('#initDate').val().length>0 || !$('#endDate').val().length>0 ) {
        if (!$('#initDate').val()) $("#controlGroup2").addClass("error");
        if (!$('#endDate').val()) $("#controlGroup2").addClass("error");
        return false; //Empty
    }
    return true;
}

//Comprueba si alguno de los inputs para las fechas tien la clase de error asociada
function checkErrorClasses() {
    if (!$("#controlGroup").hasClass("error") && !$("#controlGroup2").hasClass("error")){
        if( $('#initDate').val().length>0 && $('#endDate').val().length>0 ) {
            return false; //No error
        }
    }
    return true;
}

function isNumber(c) {
       return (/^[0-9]+$/).test(c);
}

//Document ready
$(function(){
    $("#sendButton").hide();
    $.ajaxSetup({ cache: false });

    $("#initDate").val("");
    $("#endDate").val("");
});


var path = window.location.pathname;
var urlPath=window.location.protocol+"//"+window.location.host+"/"+path.split("/",2)[1];

