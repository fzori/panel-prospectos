var 	path = window.location.pathname;
var 	urlPath=window.location.protocol+"//"+window.location.host+"/"+path.split("/",2)[1];


	// eventos
	$( "#enviar" ).click(function() {

		if( check_form() )
		{
			store_data();
		}
		

		
	});
	
	//
	// eventos
	
	$( "input" ).focus(function() {
		$(this).parent().removeClass("has-error");
	});

	// request to get checkbox options
	get_services_options();
	// request to get comercial options
	get_comercial_options();

function store_data()
{
	//tbl_prospectos_agent_id,  tbl_prospectos_email_list, tbl_prospectos_services_list
	var url = urlPath + "/formbuilder/storedata.wprl";

	$.post( url, { name: $("#InputName").val() , agent_id : $("#selected_agent").attr("selected_id")  , email_list : $("#InputEmail").val() ,  services_list : get_selected_services()  , observaciones : $("#InputMessage").val()   })
  	.done(function( data ) 
	{
		if( data == "OK" )
		{
			$( "#alertSuccess" ).show( 'slow' ).delay( 3000 ).hide( 'slow' );
			clean_form();
		}
		else
		{
			$( "#alertDanger" ).show( 'slow' ).delay( 3000 ).hide( 'slow' );
		}		

 	});

}

function clean_form()
{

	$("#InputName").val('');
	$("#selected_agent").attr("selected_id","");
	$("#InputEmail").val('')
	$("#InputMessage").val('')
	$("#selected_agent").html( "Comercial" );
	$("#service_container input").each(function (index) 
	{

		$("#" + $( this ).attr('ID') ).attr('checked', false);
         })



}
function get_comercial_options()
{
	var url = urlPath + "/formbuilder/agents.wprl";

	$.post( url, { cat_gral: "" , cat: "" , 'urlCallBack' : ""  })
  	.done(function( data ) 
	{

		$.each( data, function( key, val )
		{
			
			add_agents( val  )				
		});

		// events block
		$( ".agents_event" ).click(function() {

			$("#selected_agent").html( $(this).html()  );
			$("#selected_agent").attr( 'selected_id',  $(this).attr('id')  );
			$("#selected_agent").css( "border-color","")
			$("#selected_agent_caret").css( "border-color","")
		});
 	});

}
function add_agents( val )
{

	var 	agent = $("<li><a class='agents_event' ID='" + val.id + "' href='#'>" + val.name + "</a></li>"); 

	agent.appendTo( $('#agents_container'));
}
function get_services_options()
{
	var url = urlPath + "/formbuilder/options.wprl";

	$.post( url, { cat_gral: "" , cat: "" , 'urlCallBack' : ""  })
  	.done(function( data ) 
	{
	
		$.each( data, function( key, val )
		{
			
			add_services( val  )				
		});
		$( ".checkbox" ).click(function() 
		{
			$("#services_control").removeClass("has-error");
			$("#label-error").html("");
		});
 	});

}
function add_services( val )
{
	
var	service = $("<label class='checkbox' title='"  + val.description + "'><input ID='service_"+ val.id+ "' type='checkbox' value='"+val.tag+"'>"+val.tag+"</label>");

	service.appendTo( $('#service_container') );
}

function check_form()
{

	var 	bFilled = true;
	var 	bServices = true;
	var	bAgents = true;
	if( $("#InputName").val() == ""  )
	{
		//$("#InputName").css("background-color","red"); 
		$("#InputName").parent().addClass("has-error")
		bFilled = false;
	} 

	if( $("#InputEmail").val() == ""  )
	{
		$("#InputEmail").parent().addClass("has-error")
		bFilled = false;
	}
	else
	{
		bFilled = check_email();
		if( !bFilled )
		$("#InputEmail").parent().addClass("has-error")
	} 


	bServices = check_services();
	bAgents = check_agents();

	if( !bFilled || !bServices || !bAgents  )
	return false;
	else
	return true;

}

function get_selected_services()
{

	var selected = "";
	$("#service_container input").each(function (index) {

		if( $("#" + $( this ).attr('ID') ).is(':checked')) 
		{
			
			selected = selected + $( this ).attr('ID').replace('service_','')+ ';';
		} 

          })

	return selected.substring( 0 , selected.length-1);
}
function check_services()
{

	var 	bChecked = false;
	$("#service_container input").each(function (index) {

		if( $("#" + $( this ).attr('ID') ).is(':checked')) 
		{
			bChecked = true;

		} 

          })
	if( !bChecked )
	{
		$("#services_control").addClass("has-error");
		$("#label-error").html("Seleccione un servicio");
		bChecked = false;
	}
	return bChecked;
}
function check_agents()
{

	var 	bChecked = true;
	if( $("#selected_agent").attr("selected_id") == "" )
	{
		$("#selected_agent").css( "border-color","#a94442")
		$("#selected_agent_caret").css( "border-color","#a94442")
		bChecked = false;
	}

	return bChecked;
}
function check_email()
{

	var 	bCheck = true;
	var	auxEmail = $("#InputEmail").val().split(';');
	$.each( auxEmail , function( index, value )
	{
		if( !validateEmail( value ) )
		{
			bCheck =  false;
		}
	});
	

	return bCheck;

}

function validateEmail( $email ) 
{
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) )
  {
    return false;
  } else {
    return true;
  }
}

