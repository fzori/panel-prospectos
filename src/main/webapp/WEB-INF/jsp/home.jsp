<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%><%
%><%@taglib uri="http://www.springframework.org/tags" prefix="spring"%><%
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><!DOCTYPE html>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<jsp:include page="includes/header.jsp" />

	<title>DNIe <spring:message code='label.home.title' text='Home'/></title>
</head>

<body>

    <jsp:include page="includes/menu.jsp" />

    <div class="container">
	</div>

</body>

</html>