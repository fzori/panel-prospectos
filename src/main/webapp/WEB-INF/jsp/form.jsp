<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%><%
%><%@taglib uri="http://www.springframework.org/tags" prefix="spring"%><%
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%%><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Panel Gestión</title>

    <!-- Bootstrap core CSS -->
    <link type="text/css" rel="stylesheet" href="<c:url value='/dist/css/bootstrap.min.css'/>">
    <link  href="<c:url value='/dist/css/bootstrap-theme.min.css'/>" rel="stylesheet" >
    
	<style type="text/css">
	.alert
	{
		position: absolute;
		top: 60%;
		left: 50%;
		z-index: 10;
		display:none;
	}
	</style>
  </head>

  <body role="document">
       <div class="container theme-showcase" role="main">
      <div class="jumbotron">
        <h3>Panel Gestión Comercial</h3>

      </div>

<div class="container">
  <div class="col-md-16">

  <form role="form" action="" method="post" >
    <div class="col-lg-4">
      <div class="form-group">
        <div class="input-group">
          <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Nombre Prospecto" required>
          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
      </div>
      <div class="form-group">
        <div class="input-group">
          <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Email" required  >
          <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span></div>
      </div>
        <div class="form-group" >
            <div class="btn-group" >
                    <button type="button" ID='selected_agent' selected_id='' class="btn btn-default btn-lg" >Comercial</button>
                    <button type="button" ID='selected_agent_caret' class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" >
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                     <ul ID='agents_container' class="dropdown-menu" role="menu">
                        
                    </ul>
                </div>
      </div>
      <div ID='services_control' class="control-group" >
        <legend > Interesados en :</legend>
          <label ID='label-error' class="col-lg-7 col-md-push-1 control-label" for="inputError1" ></label>
        
         <div ID='service_container' class="col-lg-7 col-md-push-6">
             
            
        </div><!-- /.col-lg-5 -->
      </div>
    
    
  </div>



  <hr class="featurette-divider hidden-lg">
  <div class="col-lg-7 col-md-push-1">
      <div class="form-group">
        <div class="input-group">
          <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required placeholder='Observaciones'></textarea>
          <span class="input-group-addon"></span>
        </div>
      </div>

        <div class="row">
            <div class="col-xs-12 col-md-12 form-group">
             <button class="btn btn-primary pull-right" type="button" ID='enviar' >Enviar</button>
             </div>
    </div>
  </div>
</div>
  </form>
</div>
<div id='alertSuccess' class="alert alert-success" >Información guardada correctamente</div>
<div id='alertDanger' class="alert alert-danger" >Error. No se pudo guardar la información.</div>

    	    <script src="<c:url value='/js/jquery.js'/>" ></script>
    	    <script src="<c:url value='/dist/js/bootstrap.min.js'/>" ></script>
	   		<script src="<c:url value='/js/formbuilder.js'/>" ></script>  			    
  </body>
</html>
