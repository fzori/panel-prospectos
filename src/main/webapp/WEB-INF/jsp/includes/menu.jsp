<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="brand" href="#"><img src="<spring:url value='/img/redes_logo.png'/>"/></a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<sec:authorize ifAnyGranted="USER, SUPERADMIN">
						     <li class="dropdown <c:if test='${menu == \'calldaily\'}'>active</c:if>">
						        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code='label.menu.calldailyreport' text='Inf. diario llamada'/><b class="caret"></b></a>
				                <ul class="dropdown-menu">
				                  <li><a href="<spring:url value='/calldailyreport/total.wprl'/>">Totales</a></li>
				                  <li><a href="<spring:url value='/calldailyreport/dailyprofile.wprl'/>">Perfil horario</a></li>
				                </ul>
				            </li>
						</sec:authorize>
						<sec:authorize ifAnyGranted="USER, SUPERADMIN">
							<li class="dropdown <c:if test='${menu == \'operation\'}'>active</c:if>">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code='label.menu.operationdailyreport' text='Inf. diario operaci&oacute;n'/><b class="caret"></b></a>
				                <ul class="dropdown-menu">
				                  <li><a href="<spring:url value='/operationdailyreport/monthly.wprl'/>">Mensual</a></li>
				                  <li><a href="<spring:url value='/operationdailyreport/accumulated.wprl'/>">Acumulado</a></li>
				                </ul>
				            </li>
						</sec:authorize>
						<sec:authorize ifAnyGranted="USER, SUPERADMIN">
							<li <c:if test='${menu == \'audios\'}'>class="active"</c:if> >
								<a href="<spring:url value='/audios/audiofilter.wprl'/>">Consultar Audios</a>
							</li>
						</sec:authorize>
					</ul>
					
					<ul class="nav pull-right">
						<li class="divider-vertical"></li>
						<li class="pull-right"><a href="<c:url value='/logout'/>"><i class="icon-off"></i>&nbsp;<spring:message code='label.menu.logout' text="Salir"/></a></li>
					</ul>
					
				</div>
			
			</div>
     </div>
</div>