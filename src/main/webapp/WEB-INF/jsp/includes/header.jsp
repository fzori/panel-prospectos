<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><link type="text/css" rel="stylesheet" href="<c:url value='/css/bootstrap.css'/>">
<link type="text/css" rel="stylesheet" href="<c:url value='/css/default.css'/>">
<link type="text/css" rel="stylesheet" href="<c:url value='/css/datepicker.css'/>">
<link rel="shortcut icon" href="../ico/favicon.png">

<script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/bootstrap-datepicker.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.flot.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.flot.categories.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/application.js'/>"></script>
<%-- <script type="text/javascript" src="<c:url value='/i18n/${pageContext.response.locale}.js'/>"></script> --%>