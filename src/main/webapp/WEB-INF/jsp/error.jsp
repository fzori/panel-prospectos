<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%><%
%><%@taglib uri="http://www.springframework.org/tags" prefix="spring"%><%
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><!DOCTYPE html>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<jsp:include page="includes/header.jsp" />

	<title>DNIe <spring:message code='label.error.title' text='Error'/></title>
</head>

<body>

	<jsp:include page="includes/menu.jsp" />

	<div id="main-container" class="container">	

	 	<div id="body" class="row-fluid">
			<div class="span12">
			
				<div id="legend">
					<legend><spring:message code='label.error.legend' text='Error'/></legend>
				</div>
				<c:if test='${not empty param.error}'>
				<center>
				<br>
					<div class="alert alert-error">
						<c:if test='${param.typeError=="html"}'>
						<!-- Si nos viene del tipo Error -->
							<spring:message code='label.common.error.html.${param.error}' text='Error en la Aplicaci&oacute;n'/>.
						</c:if>
						<c:if test='${param.typeError != "html"}'>
							<spring:message code='label.common.error.${param.error}' text='Error en la Aplicaci&oacute;n'/>
						</c:if>
					</div>
				</center>
				</c:if>
				<c:if test='${empty param.error}'>
					<center>
						<br>
						<div class="alert alert-error">
							<spring:message code='label.error.message' text='Ha ocurrido un error durante la ejecuci&oacute;n de la aplicaci&oacute;n. Pongase en contacto con la administraci&oacute;n del sistema'/>.
						</div>
					</center>
				</c:if>
			
			</div>
		</div>

	</div>

</body>

</html>