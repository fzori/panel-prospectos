<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%><%
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%
%><%@taglib uri="http://www.springframework.org/tags" prefix="spring"%><%
%><!DOCTYPE html>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<jsp:include page="includes/header.jsp" />


	<title>DNIe <spring:message code='label.login.title' text='Acceso'/></title>
</head>

<body id="login">
	
	<div class="main-container">
				
		<form class="loginForm" action="<spring:url value='/login'/>" method="POST">
			<h3><spring:message code='label.login.title' text='Acceso'/></h3>
			<c:if test="${message != null}">
				<label id="serverError" class="text-error"><spring:message code='${message}'/></label>
			</c:if>
			
			<div class="control-group">
				<div class="controls">
<%-- 					<span class="help-inline"><spring:message code='label.login.error.user' text='Es necesario introducir un usuario'/></span> --%>
					<input id="user" class="input-block-level" name="j_username" type="text" id="usuario" placeholder="<spring:message code='label.login.user' text='Usuario'/>" <c:if test='${username != null}'>value="<c:out value='${username}'/>"</c:if>/> 
				</div>
			</div>
			
			<div class="control-group">
				<div class="controls">
<%-- 					<span class="help-inline"><spring:message code='label.login.error.pass' text='Es necesario introducir una contrase&ntilde;a'/></span> --%>
					<input id="pass" class="input-block-level" name="j_password" type="password" id="inputPassword" placeholder="<spring:message code='label.login.password' text='Contrase&ntilde;a'/>"/>
				</div>
			</div>
			<input id="submit" class="btn  btn-large btn-block btn-primary" type="submit" value="<spring:message code='label.login.submit' text='Acceder'/>"/>
			
				<c:if test='${not empty param.sessionTimeout}'>
					<center>
						<br>
						<div class="alert alert-error">
							<spring:message code='label.login.error.sessionTimeout' text='Su sesi&oacuten ha expirado o no est&aacute; logueado'/>
						</div>
					</center>
				</c:if>
		</form>
	</div>

</body>

</html>