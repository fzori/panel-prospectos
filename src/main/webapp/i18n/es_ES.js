var label = {
		"common": {
			"campaign": {
				"status":{
					"NOT_READY": "NO PREPARADA",
					"READY": "PREPARADA",
					"RUNNING": "EN EJECUCI&Oacute;N",
					"PAUSE": "PAUSADA",
					"FINISHED": "FINALIZADA"
				},
				"destination":{
					"various": "Varios"
				}
			},
			"number": {
				"status":{
					"READY": "PREPARADO",
					"STARTING": "CONFIGURANDO",
					"DIALING": "MARCANDO",
					"SUCCESS": "EN CURSO",
					"BUSY": "OCUPADO",
					"NO_ANSWER": "NO CONTESTA",
					"FAILED": "ERROR DE CONEXI&Oacute;N",
					"USER_HUNGUP": "COLGADO POR USUARIO",
					"BUSY_AGENTS": "AGENTES OCUPADOS",
					"NO_ANSWER_AGENTS": "ABANDONADA",
					"FAILED_AGENTS": "NO CONECTADO A AGENTES",
					"FINISHED": "CONTACTADO"
				}
			},
			"tags": {
				"TOTAL":"TOTAL",
				"NORESULTS":"SIN RESULTADOS"
			}
		},
		"campaignList": {
			"showSubCampaigns": "Mostrar subcampa&ntilde;as",
			"showDetails": "Mostrar detalles",
			"deleteCampaign": "Eliminar Campa&ntilde;a",
			"launchCampaign": "Lanzar campa&ntilde;a" 
		},
		"monitorizacion": {
			"launchErrorNoAgents": "No se ha podido lanzar la campa&ntilde;a. No se disponen de agentes registrados.",
			"launchErrorOutOfTime": "No se ha podido lanzar la campa&ntilde;a. La campa&ntilde;a se encuentra fuera de horario.",
			"launchErrorOutOfDate": "&Uacute;nicamente es posible volver a lanzar la campa&ntilde;a en el mismo d&iacute;a que se planificaron."
		}
};
