package es.wpr.panelprospectos.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/home")
public class HomeController {

	private final Logger logger = Logger.getLogger(HomeController.class);
	
	/**
	 * Controllador para el login en la aplicacion
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/login.wprl")
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) throws Exception {

		logger.info("Inicio login de HomeController new");
		
		ModelAndView mv =  new ModelAndView("form");
	
		
		return mv;
	}

	
	/** 
	 * Controllador para el login en la aplicacion
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/errorLogin.wprl")
	public ModelAndView errorLogin() throws Exception {

		logger.info("Inicio errorLogin de HomeController");

		ModelAndView mv = new ModelAndView("login");
		mv.addObject("message", "label.login.error");
		return mv;
	}

	
	/**
	 * Controlador para el logout
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/logout.wprl")
	public ModelAndView logout() throws Exception {

		logger.info("Inicio de logout");
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		
		session.invalidate();

		ModelAndView mv = new ModelAndView("login");
		mv.addObject("message", "label.login.logout");
		
		return mv;
	}
	
		
	
	

	@RequestMapping(value = "/error")
	public ModelAndView error() {

		return new ModelAndView("error");
	}
	
	
	


}
