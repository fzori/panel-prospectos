package es.wpr.panelprospectos.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import es.wpr.panelprospectos.dao.FormDao;
import es.wpr.panelprospectos.model.AgentTags;
import es.wpr.panelprospectos.model.FormTags;
import es.wpr.panelprospectos.model.Prospectos;



@Controller
@RequestMapping("/formbuilder")
public class FormBuilderController {
	
	@Autowired
	private FormDao formdao;
	private final Logger logger = Logger.getLogger( FormBuilderController.class );

	@RequestMapping("/options.wprl")
	@ResponseBody
	public  List<FormTags> getTags() {

		logger.info("Inicio getTag");

		List <FormTags> formtags = null;

		try {
			formtags = formdao.findTag();
		} catch (Exception e) {
			logger.error("Ha habido un error al intentar buscar los tags de incidencias Error: "+e.getMessage() );
		}
		
		
        if ( formtags  == null) {
    		logger.info("error en captura");

        }

        return  formtags;
	}
	
	@RequestMapping("/agents.wprl")
	@ResponseBody
	public  List<AgentTags> getAgents() {

		logger.info("Inicio agents");

		List <AgentTags> agenttags = null;

		try
		{
			agenttags = formdao.findAgents();
		} catch (Exception e) 
		{
			logger.error( "Ha habido un error al intentar buscar los comerciales Error: " + e.getMessage() );
		}
		
		
        if ( agenttags  == null) 
        {
    		logger.info("error en captura");

        }

        return  agenttags;
	}
	
	@RequestMapping("/storedata.wprl")
	@ResponseBody
	public  String storeData( HttpServletRequest request, HttpServletResponse response ) {
		String resp = "OK";
		Prospectos prospectos = new Prospectos();

		try 
		{
			 prospectos.setTblprospectosobservaciones(  request.getParameter("observaciones") );
			 prospectos.setTblprospectosname( request.getParameter("name") );
			 prospectos.setTblprospectosemaillist( request.getParameter("email_list") );
			 prospectos.setTblprospectosserviceslist( request.getParameter("services_list") );
			 prospectos.setTblprospectosagentid( Integer.parseInt(request.getParameter("agent_id") ) );

			formdao.insert( prospectos );
			logger.info("Vemos NOMBRE " + prospectos.getTblprospectosname()  );

		} catch (Exception e) {
			logger.error("Ha habido un error al insertar : "+e.getMessage() , e );
			resp = "KO";
		}

        return  resp;
	}
}


