package es.wpr.panelprospectos.model;

import java.util.Date;

public class Prospectos {

    private Date date;
    private Integer tblprospectosagentid;
	private String tblprospectosemaillist;
	private String tblprospectosobservaciones;
	private String tblprospectosname;
	private String tblprospectosserviceslist;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getTblprospectosagentid() {
		return tblprospectosagentid;
	}
	public void setTblprospectosagentid(Integer tblprospectosagentid) {
		this.tblprospectosagentid = tblprospectosagentid;
	}
	public String getTblprospectosobservaciones() {
		return tblprospectosobservaciones;
	}
	public void setTblprospectosobservaciones(String tblprospectosobservaciones) {
		this.tblprospectosobservaciones = tblprospectosobservaciones;
	}
	public String getTblprospectosemaillist() {
		return tblprospectosemaillist;
	}
	public void setTblprospectosemaillist(String tblprospectosemaillist) {
		this.tblprospectosemaillist = tblprospectosemaillist;
	}
	public String getTblprospectosname() {
		return tblprospectosname;
	}
	public void setTblprospectosname(String tblprospectosname) {
		this.tblprospectosname = tblprospectosname;
	}
	public String getTblprospectosserviceslist() {
		return tblprospectosserviceslist;
	}
	public void setTblprospectosserviceslist(String tblprospectosserviceslist) {
		this.tblprospectosserviceslist = tblprospectosserviceslist;
	}
	
}
