package es.wpr.panelprospectos.dao;

import java.util.List;

import es.wpr.panelprospectos.model.AgentTags;
import es.wpr.panelprospectos.model.FormTags;
import es.wpr.panelprospectos.model.Prospectos;

public interface FormDao {
	public  List<FormTags> findTag( );
	public  List<AgentTags> findAgents( );
	public int insert(Prospectos prospectos);


}
