package es.wpr.dnie.jxls;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import es.wpr.dnie.controllers.CallDailyReportController;
import es.wpr.dnie.model.DailyCdrStats;
import es.wpr.dnie.services.CallDailyReportService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TestJXLSAPI {
	
	@Configuration
	static class CallDailyReportControllerTestConfiguration {
		
	}
	
	@Test
	public void testJXLSAPI() throws Exception {
		List dailystats = new ArrayList<DailyCdrStats>();
		
		for (int i = 0; i < 10; i++) {
			DailyCdrStats d = new DailyCdrStats();
			d.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
			d.setReceived(i+10);
			d.setReceivedinhour(i);
			dailystats.add(d);
		}

		Map beans = new HashMap();

		beans.put("dailystats", dailystats);

//		System.out.println( this.getClass().getClassLoader().getResource(".").getPath()+"/prueba.xls");
		
		XLSTransformer transformer = new XLSTransformer();
		transformer.transformXLS(this.getClass().getResource("test.xls").getFile(), beans, this.getClass().getClassLoader().getResource(".").getPath()+"/prueba.xls");
	

	}

}
