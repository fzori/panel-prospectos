package es.wpr.dnie.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import es.wpr.dnie.controllers.CallDailyReportController;
import es.wpr.dnie.dtos.DailyCdrStatsDTO;
import es.wpr.dnie.services.CallDailyReportService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration
public class CallDailyReportControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private CallDailyReportController callDailyReportController;
	
	@Autowired
	private CallDailyReportService callDailyReportService;
	
	
	// Datos de las pruebas
	private String fechaInicio;
	private String fechaFin;
	private DailyCdrStatsDTO cdr;
	
	@Configuration
	static class CallDailyReportControllerTestConfiguration {
		
		@Bean
		public CallDailyReportController callDailyReportController() {
			return new CallDailyReportController();
		}

		@Bean
		public CallDailyReportService callDailyReportService() {
			return mock(CallDailyReportService.class);
		}
		
	}
	
	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(this.callDailyReportController).build();
		
		this.fechaInicio = "01-11-2013";
		this.fechaFin = "30-11-2013"; 
		this.cdr = new DailyCdrStatsDTO();
		List<String> fechas = new ArrayList<String>();
		fechas.add("15-11-2013");
		cdr.setDate(fechas);
		
		when(this.callDailyReportService.getTotal(this.fechaInicio, this.fechaFin)).thenReturn(this.cdr);
	}
	
	@Test
	public void getDailyCdrStatsTableTest() throws Exception {
		mockMvc.perform(post("/calldailyreport/showtable.wprl"))
		.andExpect(status().isOk());
	}

}
